package com.expert.validation;

import com.expert.model.NewsRecord;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class NewsRecordValidator implements Validator {
	
	private final static String TITLE = "title";
    private final static String USER_ID = "user";

	@Override
	public boolean supports(Class<?> clazz) {
		return NewsRecord.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		NewsRecord newsRecord = (NewsRecord) target;
		
		String title = newsRecord.getTitle();

		ValidationUtils.rejectIfEmpty(errors, TITLE, "news.title.empty");

//        Integer userId = newsRecord.getUser().getId();
//		if (userId != null && userId < 1) {
//			errors.rejectValue(USER_ID, "news.user.lessThenOne");
//        }
	}

}
