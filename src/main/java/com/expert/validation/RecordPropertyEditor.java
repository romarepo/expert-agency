package com.expert.validation;

import com.expert.model.User;
import com.expert.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

@Component
public class RecordPropertyEditor extends PropertyEditorSupport {

    @Autowired
    @Qualifier("userService")
    private IService<User> daoService;

    @Override
    public void setAsText(String text) {
        if (text.equals("null")) {
            return;
        }
        User user = daoService.findById(Integer.parseInt(text));
        setValue(user);
    }

    @Override
    public String getAsText() {
        return getValue() == null ? null : ((User)getValue()).getName();
    }
}
