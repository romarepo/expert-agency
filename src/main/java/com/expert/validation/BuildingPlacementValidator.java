package com.expert.validation;

import com.expert.model.BuildingPlacement;
import com.expert.model.IGeoPoint;
import com.expert.model.OfficePlacement;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BuildingPlacementValidator implements Validator {
	
	private final static String USER_ID = "emplNumber";

	@Override
	public boolean supports(Class<?> clazz) {
		return OfficePlacement.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
        BuildingPlacement buildingPlacement = (BuildingPlacement) target;
		
		Integer userId = buildingPlacement.getUser().getId();

		ValidationUtils.rejectIfEmpty(errors, USER_ID, "buildingPlacement.userId.empty");
		
		if (userId != null && userId < 1) {
			errors.rejectValue(USER_ID, "buildingPlacement.userId.lessThenOne");
        }
	}

}
