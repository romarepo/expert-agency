package com.expert.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "trading_placements")
@PrimaryKeyJoinColumn(name="placement_id")
public class TradingPlacement extends BuildingPlacement {

}
