package com.expert.model;

public enum Role {

	ADMIN("Администратор"), STUFF("Работник");
	
	private String title;
	
	private Role(String title) {
		this.setTitle(title);
	}

	public String getTitle() {
		return title;
	}

	private void setTitle(String title) {
		this.title = title;
	}
}
