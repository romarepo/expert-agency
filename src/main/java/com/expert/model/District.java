package com.expert.model;

public enum District {
    SIPAILOVO("Сипайлово"), ZELENAYA_ROZHA("Зеленая роща"), CENTER("Центр");

    private String title;

    private District(String title) {
        this.title = title;
    }

    public int getId() {
        return this.ordinal();
    }

    public String getTitle() {
        return this.title;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
