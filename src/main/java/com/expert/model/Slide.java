package com.expert.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "slides")
public class Slide implements IModel {

	@Id
	@GeneratedValue
	private Integer id;

	private String title;

	private String uri;
	
	@Column(name = "date_added")
	public Date dateCreation;
	
	public boolean isDeleted;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
	private User user;

    @Column(name = "picture_uri")
    private String pictureUri;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(String pictureUri) {
        this.pictureUri = pictureUri;
    }
}
