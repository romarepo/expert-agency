package com.expert.model;

import javax.persistence.*;

@Entity
@Table(name = "office_placements")
@PrimaryKeyJoinColumn(name="placement_id")
public class OfficePlacement extends BuildingPlacement {

}
