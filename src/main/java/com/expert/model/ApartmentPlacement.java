package com.expert.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "apartment_placements")
@PrimaryKeyJoinColumn(name="placement_id")
public class ApartmentPlacement extends BuildingPlacement {

}
