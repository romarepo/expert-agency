package com.expert.model;

public enum Type {
    APARTMENT("Квартира", "apartmentPlacement"),
    HOME("Дом", "homePlacement"),
    LAND("Земельный участок", "landPlacement"),

    OFFICE("Офис", "officePlacement"),
    STORAGE("Склады", "storagePlacement"),
    TRADING("Торговые помещения", "tradingPlacement");

    private String title;

    private String uri;

    private Type(String title, String uri) {
        this.title = title;
        this.uri = uri;
    }

    public int getId() {
        return this.ordinal();
    }

    public String getTitle() {
        return this.title;
    }

    public String getUri() {
        return uri;
    }

    @Override
    public String toString() {
        return this.title;
    }

    public static Type[] commerce() {
        Type[] types = { OFFICE, STORAGE, TRADING };
        return types;
    }

    public static Type[] personal() {
        Type[] types = { APARTMENT, HOME, LAND };
        return types;
    }
}
