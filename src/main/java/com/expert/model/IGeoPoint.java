package com.expert.model;

public interface IGeoPoint {
    public City getCity();

    public void setCity(City city);

    public District getDistrict();

    public void setDistrict(District district);

    public String getStreet();

    public void setStreet(String street);

    public Integer getBuilding();

    public void setBuilding(Integer building);
}
