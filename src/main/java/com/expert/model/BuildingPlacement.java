package com.expert.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "building_placements")
@Inheritance(strategy=InheritanceType.JOINED)
public class BuildingPlacement implements IModel, IGeoPoint {

	@Id
	@GeneratedValue
	private Integer id;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="pictures", joinColumns=@JoinColumn(name="building_placement_id"))
    @Column(name="picture_uri")
    private Collection<String> pictureUri;

    @Column(name = "presentation_uri")
    private String presentationUri;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Enumerated(EnumType.STRING)
    private City city;

    @Enumerated(EnumType.STRING)
    private District district;

    private String street;

    private Integer building;

    private Integer square;

    private Integer price;

    private String text;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getBuilding() {
        return building;
    }

    public void setBuilding(Integer building) {
        this.building = building;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSquare() {
        return square;
    }

    public void setSquare(Integer square) {
        this.square = square;
    }

    public Collection<String> getPictureUri() {
        return pictureUri;
    }

    public void setPictureUri(Collection<String> pictureUri) {
        this.pictureUri = pictureUri;
    }

    public String getPresentationUri() {
        return presentationUri;
    }

    public void setPresentationUri(String presentationUri) {
        this.presentationUri = presentationUri;
    }
}
