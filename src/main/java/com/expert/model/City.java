package com.expert.model;

public enum City {
    UFA("Уфа"), STR("Стерлитамак"), SALAVAT("Салават");

    private String title;

    private City(String title) {
        this.title = title;
    }

    public int getId() {
        return this.ordinal();
    }

    public String getTitle() {
        return this.title;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
