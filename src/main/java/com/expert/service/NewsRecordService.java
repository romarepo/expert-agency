package com.expert.service;

import com.expert.exception.NewsRecordNotFound;
import com.expert.model.NewsRecord;
import com.expert.repository.INewsRecordRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Qualifier("newsService")
@Service
public class NewsRecordService implements IService<NewsRecord> {
	
	@Resource
	private INewsRecordRepository newsRepo;

	@Override
	@Transactional
	public NewsRecord create(NewsRecord newsRecord) {
		return this.newsRepo.save(newsRecord);
	}
	
	@Override
	@Transactional
	public NewsRecord findById(int id) {
		return this.newsRepo.findOne(id);
	}

	@Override
	@Transactional(rollbackFor=NewsRecordNotFound.class)
	public NewsRecord delete(int id) throws NewsRecordNotFound {
		NewsRecord deletedNewsRecordPlacement = newsRepo.findOne(id);
		
		if (deletedNewsRecordPlacement == null)
			throw new NewsRecordNotFound();
		
		newsRepo.delete(deletedNewsRecordPlacement);
		// TODO:
		// deletedNewsRecordPlacement.setDeleted(true);
        // newsRepo.save(deletedNewsRecordPlacement);

		return deletedNewsRecordPlacement;
	}

	@Override
	@Transactional
	public List<NewsRecord> findAll() {
		return newsRepo.findAll();
	}

	@Override
	@Transactional(rollbackFor=NewsRecordNotFound.class)
	public void update(NewsRecord newsRecord) throws NewsRecordNotFound {
//		NewsRecord newsRecord = newsRepo.findOne(newsRecord.getId());
		if (newsRecord == null) {
			throw new NewsRecordNotFound();
        }
		
//		updatedNewsRecord.setText(newsRecord.getText());
//		updatedNewsRecord.setUser(newsRecord.getUser());

        newsRepo.save(newsRecord);
	}

}
