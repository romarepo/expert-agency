package com.expert.service;

import com.expert.model.OfficePlacement;
import com.expert.repository.IOfficePlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class OfficePlacementService extends APlacementService<OfficePlacement> {

    @Autowired
    protected IOfficePlacementRepository repo;

    public OfficePlacementService() {
        super(OfficePlacement.class);
    }

    @Override
    protected IPlacementRepository<OfficePlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public OfficePlacement create(OfficePlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}