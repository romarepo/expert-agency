package com.expert.service;

import com.expert.exception.BuildingPlacementNotFound;
import com.expert.model.BuildingPlacement;
import com.expert.model.City;
import com.expert.model.District;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Component
public abstract class APlacementService<T extends BuildingPlacement> {

    @Autowired
    private EntityManagerFactory emf;

    @PersistenceContext
    private EntityManager em;

    private Class<T> clazz;

    public APlacementService(Class clazz) {
        this.clazz = clazz;
    }

    protected abstract IPlacementRepository<T> getRepo();

    @Transactional
    public T create(T buildingPlacement) {
        return getRepo().save(buildingPlacement);
//        em.persist(buildingPlacement);
//        em.flush();
//        return buildingPlacement;
    }

    @Transactional
    public T findById(int id) {
        return getRepo().findOne(id);
//        return em.find(clazz, id);
    }

    @Transactional(rollbackFor=BuildingPlacementNotFound.class)
    public T delete(int id) throws BuildingPlacementNotFound {
        T deletedBuilding = findById(id);

        if (deletedBuilding == null)
            throw new BuildingPlacementNotFound();
//        em.remove(deletedBuilding);
        getRepo().delete(id);

        return deletedBuilding;
    }

    @Transactional(rollbackFor=BuildingPlacementNotFound.class)
    public void update(T buildingPlacement) throws BuildingPlacementNotFound {
//        em.persist(buildingPlacement);
        getRepo().save(buildingPlacement);
    }

    @Transactional
    public Collection<T> findAll() {
        return getRepo().findAll();
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<T> q = cb.createQuery(clazz);
//
//        Root<T> c = q.from(clazz);
//        q.select(c);
//
//        TypedQuery<T> query = em.createQuery(q);
//        return query.getResultList();
    }

    @Transactional
    public Collection<T> filter(City city, District district, String street, Integer building) {
        if (city == null || district == null || street == null || building == null) {
            return getRepo().findAll();
        }

        return getRepo().findByCityAndDistrictAndStreetAndBuilding(city, district, street, building);
//        Collection<T> items = findAll();
//
//        return items;
    }
}
