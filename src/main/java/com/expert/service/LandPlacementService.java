package com.expert.service;

import com.expert.model.LandPlacement;
import com.expert.repository.ILandPlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class LandPlacementService extends APlacementService<LandPlacement> {

    @Autowired
    protected ILandPlacementRepository repo;

    public LandPlacementService() {
        super(LandPlacement.class);
    }

    @Override
    protected IPlacementRepository<LandPlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public LandPlacement create(LandPlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}