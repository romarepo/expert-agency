package com.expert.service;

import com.expert.model.HomePlacement;
import com.expert.repository.IHomePlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class HomePlacementService extends APlacementService<HomePlacement> {

    @Autowired
    protected IHomePlacementRepository repo;

    public HomePlacementService() {
        super(HomePlacement.class);
    }

    @Override
    protected IPlacementRepository<HomePlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public HomePlacement create(HomePlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}