package com.expert.service;

import com.expert.model.StoragePlacement;
import com.expert.repository.IStoragePlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class StoragePlacementService extends APlacementService<StoragePlacement> {

    @Autowired
    protected IStoragePlacementRepository repo;

    public StoragePlacementService() {
        super(StoragePlacement.class);
    }

    @Override
    protected IPlacementRepository<StoragePlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public StoragePlacement create(StoragePlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}