package com.expert.service;

import com.expert.exception.SlideNotFound;
import com.expert.model.Slide;
import com.expert.repository.ISlideRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Qualifier("slideService")
@Service
public class SlideService implements IService<Slide> {
	
	@Resource
	private ISlideRepository slideRepo;

	@Override
	@Transactional
	public Slide create(Slide slide) {
		return this.slideRepo.save(slide);
	}
	
	@Override
	@Transactional
	public Slide findById(int id) {
		return this.slideRepo.findOne(id);
	}

	@Override
	@Transactional(rollbackFor=SlideNotFound.class)
	public Slide delete(int id) throws SlideNotFound {
		Slide deletedSlidePlacement = slideRepo.findOne(id);
		
		if (deletedSlidePlacement == null)
			throw new SlideNotFound();
		
		slideRepo.delete(deletedSlidePlacement);
		// TODO:
		// deletedSlidePlacement.setDeleted(true);
        // slideRepo.save(deletedSlidePlacement);

		return deletedSlidePlacement;
	}

	@Override
	@Transactional
	public List<Slide> findAll() {
		return slideRepo.findAll();
	}

	@Override
	@Transactional(rollbackFor=SlideNotFound.class)
	public void update(Slide slide) throws SlideNotFound {
//		Slide slide = slideRepo.findOne(slide.getId());
		if (slide == null) {
			throw new SlideNotFound();
        }
		
//		updatedSlide.setText(slide.getText());
//		updatedSlide.setUser(slide.getUser());

        slideRepo.save(slide);
	}

}
