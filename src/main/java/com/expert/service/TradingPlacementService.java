package com.expert.service;

import com.expert.model.TradingPlacement;
import com.expert.repository.ITradingPlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class TradingPlacementService extends APlacementService<TradingPlacement> {

    @Autowired
    protected ITradingPlacementRepository repo;

    public TradingPlacementService() {
        super(TradingPlacement.class);
    }

    @Override
    protected IPlacementRepository<TradingPlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public TradingPlacement create(TradingPlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}