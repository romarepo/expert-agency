package com.expert.service;

import com.expert.model.BuildingPlacement;
import com.expert.repository.IBuildingPlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class BuildingPlacementService extends APlacementService<BuildingPlacement> {

    @Autowired
    protected IBuildingPlacementRepository repo;

    public BuildingPlacementService() {
        super(BuildingPlacement.class);
    }

    @Override
    protected IPlacementRepository<BuildingPlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public BuildingPlacement create(BuildingPlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}