package com.expert.service;

import com.expert.exception.UserNotFound;
import com.expert.model.User;
import com.expert.repository.UserRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Qualifier("userService")
@Service
public class UserService implements IService<User> {
	
	@Resource
	private UserRepository userRepo;

	@Override
	@Transactional
	public User create(User userRecord) {
		return this.userRepo.save(userRecord);
	}
	
	@Override
	@Transactional
	public User findById(int id) {
		return this.userRepo.findOne(id);
	}

	@Override
	@Transactional(rollbackFor=UserNotFound.class)
	public User delete(int id) throws UserNotFound {
		User deletedUserPlacement = userRepo.findOne(id);
		
		if (deletedUserPlacement == null)
			throw new UserNotFound();
		
		userRepo.delete(deletedUserPlacement);
		return deletedUserPlacement;
	}

	@Override
	@Transactional
	public List<User> findAll() {
		return userRepo.findAll();
	}

	@Override
	@Transactional(rollbackFor=UserNotFound.class)
	public void update(User userRecord) throws UserNotFound {
//		User userRecord = userRepo.findOne(userRecord.getId());
		if (userRecord == null) {
			throw new UserNotFound();
        }
		
//		updatedUser.setText(userRecord.getText());
//		updatedUser.setUser(userRecord.getUser());

        userRepo.save(userRecord);
	}
}
