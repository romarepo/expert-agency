package com.expert.service;

import com.expert.model.ApartmentPlacement;
import com.expert.repository.IApartmentPlacementRepository;
import com.expert.repository.IPlacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class ApartmentPlacementService extends APlacementService<ApartmentPlacement> {

    @Autowired
    protected IApartmentPlacementRepository repo;

    public ApartmentPlacementService() {
        super(ApartmentPlacement.class);
    }

    @Override
    protected IPlacementRepository<ApartmentPlacement> getRepo() {
        return repo;
    }

    //    @Override
//    @Transactional(Transactional.TxType.REQUIRED)
//    public ApartmentPlacement create(ApartmentPlacement buildingPlacement) {
//        return super.create(buildingPlacement);
//    }
}