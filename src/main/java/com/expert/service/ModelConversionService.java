package com.expert.service;

import com.expert.model.IModel;
import com.expert.factory.ModelFormConverterFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

@Service
public class ModelConversionService implements ConversionService {

    private final Converter<String, IModel> converter = new ModelFormConverterFactory().getConverter(IModel.class);

    @Override
    public boolean canConvert(Class<?> sourceType, Class<?> targetType) {
        return sourceType.equals(java.lang.String.class) && targetType.equals(IModel.class);
    }

    @Override
    public boolean canConvert(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return sourceType.equals(java.lang.String.class) && targetType.equals(IModel.class);
    }

    @Override
    public <T> T convert(Object source, Class<T> targetType) {
        return (T) converter.convert((String) source);
    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        return converter.convert((String) source);
    }
}