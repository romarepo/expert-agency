package com.expert.service;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

public interface IService<T> {

    public T create(T item);

    public T delete(int id) throws Exception;

    public Collection<T> findAll();

    public void update(T item) throws Exception;

    public T findById(int id);

}
