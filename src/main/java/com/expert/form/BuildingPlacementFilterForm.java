package com.expert.form;

import com.expert.model.City;
import com.expert.model.District;
import com.expert.model.Type;
import com.expert.model.User;

public class BuildingPlacementFilterForm {

    private Type type;

    private User user;

    private City city;

    private District district;

    private String street;

    private Integer building;

    private Integer minSquare;
    private Integer maxSquare;

    private Integer minPrice;
    private Integer maxPrice;

    private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getBuilding() {
        return building;
    }

    public void setBuilding(Integer building) {
        this.building = building;
    }

    public Integer getMinPrice() {
        return minPrice;
    }
    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMinPrice(Integer price) {
        this.minPrice = price;
    }
    public void setMaxPrice(Integer price) {
        this.maxPrice = price;
    }

    public Integer getMinSquare() {
        return minSquare;
    }
    public Integer getMaxSquare() {
        return maxSquare;
    }

    public void setMinSquare(Integer square) {
        this.minSquare = square;
    }
    public void setMaxSquare(Integer square) {
        this.maxSquare = square;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
