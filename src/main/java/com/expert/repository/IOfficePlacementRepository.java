package com.expert.repository;

import com.expert.model.OfficePlacement;

public interface IOfficePlacementRepository extends IPlacementRepository<OfficePlacement> {
}
