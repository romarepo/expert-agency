package com.expert.repository;

import com.expert.model.TradingPlacement;

public interface ITradingPlacementRepository extends IPlacementRepository<TradingPlacement> {
}
