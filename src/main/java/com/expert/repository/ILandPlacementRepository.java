package com.expert.repository;

import com.expert.model.LandPlacement;

public interface ILandPlacementRepository extends IPlacementRepository<LandPlacement> {
}
