package com.expert.repository;

import com.expert.model.NewsRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface INewsRecordRepository extends JpaRepository<NewsRecord, Integer> {
}
