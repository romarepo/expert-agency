package com.expert.repository;

import com.expert.model.BuildingPlacement;
import com.expert.model.City;
import com.expert.model.District;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IPlacementRepository<T> extends JpaRepository<T, Integer> {

    List<T> findByCity(City city);

    List<T> findByDistrict(District district);

    List<T> findByCityAndDistrictAndStreetAndBuilding(
            City city, District district, String street, int building);
}
