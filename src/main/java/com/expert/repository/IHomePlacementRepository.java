package com.expert.repository;

import com.expert.model.HomePlacement;

public interface IHomePlacementRepository extends IPlacementRepository<HomePlacement> {
}
