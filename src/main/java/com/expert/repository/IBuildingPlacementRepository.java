package com.expert.repository;

import com.expert.model.BuildingPlacement;

public interface IBuildingPlacementRepository extends IPlacementRepository<BuildingPlacement> {
}
