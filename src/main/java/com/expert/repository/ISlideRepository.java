package com.expert.repository;

import com.expert.model.Slide;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISlideRepository extends JpaRepository<Slide, Integer> {
}
