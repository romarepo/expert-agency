package com.expert.repository;

import com.expert.model.ApartmentPlacement;

public interface IApartmentPlacementRepository extends IPlacementRepository<ApartmentPlacement> {
}
