package com.expert.repository;

import com.expert.model.StoragePlacement;

public interface IStoragePlacementRepository extends IPlacementRepository<StoragePlacement> {
}
