package com.expert.controller;

import com.expert.model.Slide;
import com.expert.model.User;
import com.expert.service.IService;
import com.expert.util.FileWriter;
import com.expert.validation.RecordPropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value="/admin/slide")
public class SlideAdminController {

	@Autowired
    @Qualifier("slideService")
	private IService<Slide> slideService;

    @Autowired
    private RecordPropertyEditor slidePropertyEditor;

	@Autowired
    @Qualifier("userService")
	private IService<User> userService;
    /**

     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public Slide slideConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new Slide() : slideService.findById(id);
    }

	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView createGet() {
		ModelAndView mav = new ModelAndView("admin-slide-create", "slide", new Slide());
        fillUsers(mav);

		return mav;
	}

    @RequestMapping(value="/create", method=RequestMethod.POST)
	public ModelAndView create(
            @ModelAttribute @Valid Slide slide,
			final BindingResult result,
			final RedirectAttributes redirectAttributes,
            HttpServletRequest request
            ,@RequestParam("file") MultipartFile file
    ) throws IOException {

		if (result.hasErrors()) {
            createGet();
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/")+"/"
                + fileName);
        slide.setPictureUri(fileName);

        slideService.create(slide);

		String message = "New slide record "
                + slide.getTitle()+" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/admin/slide/list");

		return mav;		
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView("admin-slide-list");
        Collection<Slide> slide = slideService.findAll();
		mav.addObject("slides", slide);
		return mav;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editGet(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView("admin-slide-edit");

		Slide slide = slideService.findById(id);
		mav.addObject("slide", slide);

        fillUsers(mav);
        mav.addObject("currentUser", slide.getUser());

        return mav;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute Slide slide,
			BindingResult result,
			final RedirectAttributes redirectAttributes,
            HttpServletRequest request
            ,@RequestParam("file") MultipartFile file
    ) throws IOException, Exception {

        if (result.hasErrors()) {
            editGet(slide.getId());
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/")+"/"
                        + fileName);
        slide.setPictureUri(fileName);
        slideService.update(slide);

        String message = "Slide was successfully updated.";
		redirectAttributes.addFlashAttribute("message", message);

		return editGet(slide.getId());
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView delete(@PathVariable Integer id,
			final RedirectAttributes redirectAttributes) throws Exception {

		ModelAndView mav = new ModelAndView("redirect:/admin/slide/list");
		
		Slide slide = slideService.delete(id);
		String message = "The slide "+ slide.getTitle()+" was successfully deleted.";
		redirectAttributes.addFlashAttribute("message", message);

		return mav;
	}

    public void fillUsers(ModelAndView mav) {
        Collection<User> users = userService.findAll();
        mav.addObject("users", users);
    }
}
