package com.expert.controller;

import com.expert.model.City;
import com.expert.model.District;
import com.expert.model.User;
import com.expert.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;

public abstract class BuildingPlacementController {

    @Autowired
    @Qualifier("userService")
    private IService<User> userService;

    protected final void fillUsers(ModelAndView mav) {
        Collection<User> users = userService.findAll();
        mav.addObject("users", users);
    }

    protected static final void fillCities(ModelAndView mav) {
        mav.addObject("cities", City.values());
        mav.addObject("districts", District.values());
    }
}
