package com.expert.controller;

import com.expert.form.BuildingPlacementFilterForm;
import com.expert.model.TradingPlacement;
import com.expert.service.TradingPlacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Controller
@RequestMapping(value="/tradingPlacement")
public class TradingPlacementController extends BuildingPlacementController {

    @Autowired
    protected TradingPlacementService tradingPlacementService;

    @RequestMapping(value="/show/{id}", method=RequestMethod.GET)
    public ModelAndView show(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("tradingPlacement-show");

        TradingPlacement tradingPlacement = tradingPlacementService.findById(id);
        mav.addObject("item", tradingPlacement);

        return mav;
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list(@ModelAttribute BuildingPlacementFilterForm buildingPlacementForm) {
        ModelAndView mnv = new ModelAndView("tradingPlacement-list");
        Collection<TradingPlacement> tradingPlacement =
                tradingPlacementService.filter(
                        buildingPlacementForm.getCity(), buildingPlacementForm.getDistrict(),
                        buildingPlacementForm.getStreet(), buildingPlacementForm.getBuilding());

        // form building params
        mnv.addObject("buildingPlacementForm", buildingPlacementForm);
        BuildingPlacementController.fillCities(mnv);

        mnv.addObject("items", tradingPlacement);

        return mnv;
    }
}
