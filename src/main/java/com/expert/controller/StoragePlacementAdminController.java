package com.expert.controller;

import com.expert.model.StoragePlacement;
import com.expert.model.User;
import com.expert.service.StoragePlacementService;
import com.expert.util.FileWriter;
import com.expert.validation.BuildingPlacementValidator;
import com.expert.validation.FileValidator;
import com.expert.validation.RecordPropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value="/admin/storagePlacement")
public class StoragePlacementAdminController extends BuildingPlacementController {

    @Autowired
    private StoragePlacementService storagePlacementService;

    @Autowired
    private BuildingPlacementValidator buildingPlacementValidator;

    @Autowired
    FileValidator fileValidator;

    @Autowired
    private RecordPropertyEditor storagePlacementPropertyEditor;

    /**
     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public StoragePlacement storagePlacementConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new StoragePlacement() : storagePlacementService.findById(id);
    }

    @InitBinder
    private void initBinder(WebDataBinder binder) {
//		binder.setValidator(buildingPlacementValidator);
        binder.registerCustomEditor(User.class, storagePlacementPropertyEditor);
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView mav = new ModelAndView("admin-storagePlacement-list");
        Collection<StoragePlacement> storagePlacement = storagePlacementService.findAll();
        mav.addObject("items", storagePlacement);
        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.GET)
    public ModelAndView createGet() {
        ModelAndView mnv = new ModelAndView("admin-storagePlacement-create", "item", new StoragePlacement());
        fillUsers(mnv);
        fillCities(mnv);

        return mnv;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView delete(@PathVariable Integer id,
                               final RedirectAttributes redirectAttributes) throws Exception {

        ModelAndView mav = new ModelAndView("redirect:/admin/storagePlacement/list");

        StoragePlacement storagePlacement = storagePlacementService.delete(id);
        String message = "The storagePlacement "+ storagePlacement.getStreet()+" was successfully deleted.";
        redirectAttributes.addFlashAttribute("message", message);

        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editGet(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("admin-storagePlacement-edit");

        StoragePlacement storagePlacement = storagePlacementService.findById(id);
        mav.addObject("item", storagePlacement);

        fillUsers(mav);
        fillCities(mav);

        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.POST)
    public ModelAndView create(
            @ModelAttribute @Valid StoragePlacement storagePlacement,
            final BindingResult result,
            final RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {

        if (result.hasErrors()) {
            editGet(storagePlacement.getId());
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + fileName);
        storagePlacement.setPictureUri(fileName);

        // TODO: UID get random
        String pptName = (new Date()).getTime() +"_"+ ppt.getOriginalFilename();
        FileWriter.writeMultipartFile(ppt,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + pptName);
        storagePlacement.setPresentationUri(pptName);

        storagePlacementService.create(storagePlacement);

        String message = "New storagePlacement record "
                + storagePlacement.getText()+" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/admin/storagePlacement/list");

        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edit(
            @ModelAttribute StoragePlacement storagePlacement,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {
        return create(storagePlacement, result, redirectAttributes, request, file, ppt);
    }
}
