package com.expert.controller;

import com.expert.form.BuildingPlacementFilterForm;
import com.expert.model.ApartmentPlacement;
import com.expert.service.ApartmentPlacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(value="/apartmentPlacement")
public class ApartmentPlacementController extends BuildingPlacementController {

    @Autowired
    private ApartmentPlacementService apartmentPlacementService;

    @RequestMapping(value="/show/{id}", method=RequestMethod.GET)
    public ModelAndView show(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("apartmentPlacement-show");

        ApartmentPlacement apartmentPlacement = apartmentPlacementService.findById(id);
        mav.addObject("item", apartmentPlacement);

        return mav;
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list(@ModelAttribute BuildingPlacementFilterForm buildingPlacementForm) {
        ModelAndView mnv = new ModelAndView("apartmentPlacement-list");
        Collection<ApartmentPlacement> apartmentPlacement =
                apartmentPlacementService.filter(
                        buildingPlacementForm.getCity(), buildingPlacementForm.getDistrict(),
                        buildingPlacementForm.getStreet(), buildingPlacementForm.getBuilding());

        // form building params
        mnv.addObject("buildingPlacementForm", buildingPlacementForm);
        BuildingPlacementController.fillCities(mnv);

        mnv.addObject("items", apartmentPlacement);

        return mnv;
    }
}
