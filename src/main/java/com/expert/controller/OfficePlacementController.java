package com.expert.controller;

import com.expert.form.BuildingPlacementFilterForm;
import com.expert.model.OfficePlacement;
import com.expert.model.TradingPlacement;
import com.expert.service.OfficePlacementService;
import com.expert.service.TradingPlacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Controller
@RequestMapping(value="/officePlacement")
public class OfficePlacementController extends BuildingPlacementController {

    @Autowired
    protected OfficePlacementService officePlacementService;

    @RequestMapping(value="/show/{id}", method=RequestMethod.GET)
    public ModelAndView show(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("officePlacement-show");

        OfficePlacement officePlacement = officePlacementService.findById(id);
        mav.addObject("item", officePlacement);

        return mav;
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list(@ModelAttribute BuildingPlacementFilterForm buildingPlacementForm) {
        ModelAndView mnv = new ModelAndView("officePlacement-list");
        Collection<OfficePlacement> officePlacement =
                officePlacementService.filter(
                        buildingPlacementForm.getCity(), buildingPlacementForm.getDistrict(),
                        buildingPlacementForm.getStreet(), buildingPlacementForm.getBuilding());

        // form building params
        mnv.addObject("buildingPlacementForm", buildingPlacementForm);
        BuildingPlacementController.fillCities(mnv);

        mnv.addObject("items", officePlacement);

        return mnv;
    }
}
