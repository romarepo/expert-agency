package com.expert.controller;

import com.expert.model.TradingPlacement;
import com.expert.model.User;
import com.expert.service.TradingPlacementService;
import com.expert.util.FileWriter;
import com.expert.validation.BuildingPlacementValidator;
import com.expert.validation.FileValidator;
import com.expert.validation.RecordPropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value="/admin/tradingPlacement")
public class TradingPlacementAdminController extends BuildingPlacementController {

    @Autowired
    private TradingPlacementService tradingPlacementService;

    @Autowired
    private BuildingPlacementValidator buildingPlacementValidator;

    @Autowired
    FileValidator fileValidator;

    @Autowired
    private RecordPropertyEditor tradingPlacementPropertyEditor;

    /**
     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public TradingPlacement tradingPlacementConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new TradingPlacement() : tradingPlacementService.findById(id);
    }

    @InitBinder
    private void initBinder(WebDataBinder binder) {
//		binder.setValidator(buildingPlacementValidator);
        binder.registerCustomEditor(User.class, tradingPlacementPropertyEditor);
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView mav = new ModelAndView("admin-tradingPlacement-list");
        Collection<TradingPlacement> tradingPlacement = tradingPlacementService.findAll();
        mav.addObject("items", tradingPlacement);
        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.GET)
    public ModelAndView createGet() {
        ModelAndView mnv = new ModelAndView("admin-tradingPlacement-create", "item", new TradingPlacement());
        fillUsers(mnv);
        fillCities(mnv);

        return mnv;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView delete(@PathVariable Integer id,
                               final RedirectAttributes redirectAttributes) throws Exception {

        ModelAndView mav = new ModelAndView("redirect:/admin/tradingPlacement/list");

        TradingPlacement tradingPlacement = tradingPlacementService.delete(id);
        String message = "The tradingPlacement "+ tradingPlacement.getStreet()+" was successfully deleted.";
        redirectAttributes.addFlashAttribute("message", message);

        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editGet(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("admin-tradingPlacement-edit");

        TradingPlacement tradingPlacement = tradingPlacementService.findById(id);
        mav.addObject("item", tradingPlacement);

        fillUsers(mav);
        fillCities(mav);

        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.POST)
    public ModelAndView create(
            @ModelAttribute @Valid TradingPlacement tradingPlacement,
            final BindingResult result,
            final RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {

        if (result.hasErrors()) {
            editGet(tradingPlacement.getId());
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + fileName);
        tradingPlacement.setPictureUri(fileName);

        // TODO: UID get random
        String pptName = (new Date()).getTime() +"_"+ ppt.getOriginalFilename();
        FileWriter.writeMultipartFile(ppt,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + pptName);
        tradingPlacement.setPresentationUri(pptName);

        tradingPlacementService.create(tradingPlacement);

        String message = "New tradingPlacement record "
                + tradingPlacement.getText()+" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/admin/tradingPlacement/list");

        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edit(
            @ModelAttribute TradingPlacement tradingPlacement,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {
        return create(tradingPlacement, result, redirectAttributes, request, file, ppt);
    }
}
