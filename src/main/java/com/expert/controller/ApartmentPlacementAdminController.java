package com.expert.controller;

import com.expert.model.ApartmentPlacement;
import com.expert.model.User;
import com.expert.service.ApartmentPlacementService;
import com.expert.util.FileWriter;
import com.expert.validation.BuildingPlacementValidator;
import com.expert.validation.FileValidator;
import com.expert.validation.RecordPropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value="/admin/apartmentPlacement")
public class ApartmentPlacementAdminController extends BuildingPlacementController {

    @Autowired
    private ApartmentPlacementService apartmentPlacementService;

    @Autowired
    private BuildingPlacementValidator buildingPlacementValidator;

    @Autowired
    FileValidator fileValidator;

    @Autowired
    private RecordPropertyEditor apartmentPlacementPropertyEditor;

    /**
     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public ApartmentPlacement apartmentPlacementConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new ApartmentPlacement() : apartmentPlacementService.findById(id);
    }

    @InitBinder
    private void initBinder(WebDataBinder binder) {
//		binder.setValidator(buildingPlacementValidator);
        binder.registerCustomEditor(User.class, apartmentPlacementPropertyEditor);
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView mav = new ModelAndView("admin-apartmentPlacement-list");
        Collection<ApartmentPlacement> apartmentPlacement = apartmentPlacementService.findAll();
        mav.addObject("items", apartmentPlacement);
        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.GET)
    public ModelAndView createGet() {
        ModelAndView mnv = new ModelAndView("admin-apartmentPlacement-create", "item", new ApartmentPlacement());
        fillUsers(mnv);
        fillCities(mnv);

        return mnv;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView delete(@PathVariable Integer id,
                               final RedirectAttributes redirectAttributes) throws Exception {

        ModelAndView mav = new ModelAndView("redirect:/admin/apartmentPlacement/list");

        ApartmentPlacement apartmentPlacement = apartmentPlacementService.delete(id);
        String message = "The apartmentPlacement "+ apartmentPlacement.getStreet()+" was successfully deleted.";
        redirectAttributes.addFlashAttribute("message", message);

        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editGet(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("admin-apartmentPlacement-edit");

        ApartmentPlacement apartmentPlacement = apartmentPlacementService.findById(id);
        mav.addObject("item", apartmentPlacement);

        fillUsers(mav);
        fillCities(mav);

        return mav;
    }

    @RequestMapping(value="/create", method=RequestMethod.POST)
    public ModelAndView create(
            @ModelAttribute @Valid ApartmentPlacement apartmentPlacement,
            final BindingResult result,
            final RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {

        if (result.hasErrors()) {
            editGet(apartmentPlacement.getId());
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + fileName);
        apartmentPlacement.setPictureUri(fileName);

        // TODO: UID get random
        String pptName = (new Date()).getTime() +"_"+ ppt.getOriginalFilename();
        FileWriter.writeMultipartFile(ppt,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + pptName);
        apartmentPlacement.setPresentationUri(pptName);

        apartmentPlacementService.create(apartmentPlacement);

        String message = "New apartmentPlacement record "
                + apartmentPlacement.getText()+" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/admin/apartmentPlacement/list");

        return mav;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edit(
            @ModelAttribute ApartmentPlacement apartmentPlacement,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {
        return create(apartmentPlacement, result, redirectAttributes, request, file, ppt);
    }
}
