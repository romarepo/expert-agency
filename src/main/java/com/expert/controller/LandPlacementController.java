package com.expert.controller;

import com.expert.form.BuildingPlacementFilterForm;
import com.expert.model.LandPlacement;
import com.expert.service.LandPlacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Controller
@RequestMapping(value="/landPlacement")
public class LandPlacementController extends BuildingPlacementController {

    @Autowired
    protected LandPlacementService landPlacementService;

    @RequestMapping(value="/show/{id}", method=RequestMethod.GET)
    public ModelAndView show(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("landPlacement-show");

        LandPlacement landPlacement = landPlacementService.findById(id);
        mav.addObject("item", landPlacement);

        return mav;
    }

    @RequestMapping(value="/list", method=RequestMethod.GET)
    public ModelAndView list(@ModelAttribute BuildingPlacementFilterForm buildingPlacementForm) {
        ModelAndView mnv = new ModelAndView("landPlacement-list");
        Collection<LandPlacement> landPlacement =
                landPlacementService.filter(
                        buildingPlacementForm.getCity(), buildingPlacementForm.getDistrict(),
                        buildingPlacementForm.getStreet(), buildingPlacementForm.getBuilding());

        // form building params
        mnv.addObject("buildingPlacementForm", buildingPlacementForm);
        BuildingPlacementController.fillCities(mnv);

        mnv.addObject("items", landPlacement);

        return mnv;
    }
}
