package com.expert.controller;

import com.expert.model.Role;
import com.expert.model.User;
import com.expert.service.IService;
import com.expert.util.FileWriter;
import com.expert.validation.FileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value="/admin/user")
public class UserAdminController {

	@Autowired
    @Qualifier("userService")
	private IService<User> userService;

    @Autowired
    FileValidator fileValidator;

    /**

     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public User userConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new User() : userService.findById(id);
    }

	@InitBinder
	private void initBinder(WebDataBinder binder) {
//		binder.setValidator(userValidator);
//        binder.registerCustomEditor(User.class, userPropertyEditor);
	}

	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView createGet() {
		ModelAndView mav = new ModelAndView("admin-user-create", "user", new User());
        fillRoles(mav);

		return mav;
	}

    @RequestMapping(value="/create", method=RequestMethod.POST)
	public ModelAndView create(
            @ModelAttribute @Valid User user,
			final BindingResult result,
			final RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file
    ) throws IOException {

		if (result.hasErrors()) {
            createGet();
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + fileName);
        user.setPictureUri(fileName);

        userService.create(user);

		String message = "New user "
                + user.getName() + user.getLastName() +" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/admin/user/list");

		return mav;		
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView("admin-user-list");

        Collection<User> user = userService.findAll();
		mav.addObject("users", user);

        fillRoles(mav);

		return mav;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editGet(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView("admin-user-edit");

		User user = userService.findById(id);
		mav.addObject("user", user);

        fillRoles(mav);

        return mav;
	}

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute User user,
			BindingResult result,
			final RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file
    ) throws IOException, Exception {

        if (result.hasErrors()) {
            editGet(user.getId());
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/")+"/"
                        + fileName);
        user.setPictureUri(fileName);
        userService.update(user);

        String message = "User was successfully updated.";
		redirectAttributes.addFlashAttribute("message", message);

		return editGet(user.getId());
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView delete(@PathVariable Integer id,
			final RedirectAttributes redirectAttributes) throws Exception {

		ModelAndView mav = new ModelAndView("redirect:/admin/user/list");
		
		User user = userService.delete(id);
		String message = "The user "+ user.getName() + user.getLastName() +" was successfully deleted.";
		redirectAttributes.addFlashAttribute("message", message);

		return mav;
	}

    private void fillRoles(ModelAndView mav) {
        mav.addObject("roles", Role.values());
    }
}
