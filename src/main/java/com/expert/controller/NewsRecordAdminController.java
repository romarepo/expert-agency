package com.expert.controller;

import com.expert.model.NewsRecord;
import com.expert.model.User;
import com.expert.service.IService;
import com.expert.util.FileWriter;
import com.expert.validation.RecordPropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping(value="/admin/news")
public class NewsRecordAdminController {

	@Autowired
    @Qualifier("newsService")
	private IService<NewsRecord> newsService;

    @Autowired
    private RecordPropertyEditor newsPropertyEditor;

	@Autowired
    @Qualifier("userService")
	private IService<User> userService;
    /**

     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public NewsRecord newsRecordConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new NewsRecord() : newsService.findById(id);
    }

	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView createGet() {
		ModelAndView mav = new ModelAndView("admin-news-create", "newsRecord", new NewsRecord());
        fillUsers(mav);

		return mav;
	}

    @RequestMapping(value="/create", method=RequestMethod.POST)
	public ModelAndView create(
            @ModelAttribute @Valid NewsRecord newsRecord,
			final BindingResult result,
			final RedirectAttributes redirectAttributes,
            HttpServletRequest request
            ,@RequestParam("file") MultipartFile file
    ) throws IOException {

		if (result.hasErrors()) {
            createGet();
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/")+"/"
                + fileName);
        newsRecord.setPictureUri(fileName);

        newsService.create(newsRecord);

		String message = "New news record "
                + newsRecord.getText()+" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/admin/news/list");

		return mav;		
	}
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView("admin-news-list");
        Collection<NewsRecord> news = newsService.findAll();
		mav.addObject("newsRecords", news);
		return mav;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView editGet(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView("admin-news-edit");

		NewsRecord newsRecord = newsService.findById(id);
		mav.addObject("newsRecord", newsRecord);

        fillUsers(mav);
        mav.addObject("currentUser", newsRecord.getUser());

        return mav;
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute NewsRecord newsRecord,
			BindingResult result,
			final RedirectAttributes redirectAttributes,
            HttpServletRequest request
            ,@RequestParam("file") MultipartFile file
    ) throws IOException, Exception {

        if (result.hasErrors()) {
            editGet(newsRecord.getId());
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/")+"/"
                        + fileName);
        newsRecord.setPictureUri(fileName);
        newsService.update(newsRecord);

        String message = "NewsRecord was successfully updated.";
		redirectAttributes.addFlashAttribute("message", message);

		return editGet(newsRecord.getId());
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public ModelAndView delete(@PathVariable Integer id,
			final RedirectAttributes redirectAttributes) throws Exception {

		ModelAndView mav = new ModelAndView("redirect:/admin/news/list");
		
		NewsRecord news = newsService.delete(id);
		String message = "The newsRecord "+ news.getTitle()+" was successfully deleted.";
		redirectAttributes.addFlashAttribute("message", message);

		return mav;
	}

    public void fillUsers(ModelAndView mav) {
        Collection<User> users = userService.findAll();
        mav.addObject("users", users);
    }
}
