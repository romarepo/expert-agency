package com.expert.controller;

import com.expert.model.*;
import com.expert.service.BuildingPlacementService;
import com.expert.util.FileWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;

@Controller
public class RequestBuildingPlacementController extends BuildingPlacementController {

    @Autowired
    private BuildingPlacementService buildingPlacementService;

    @RequestMapping(value="/request/create", method=RequestMethod.GET)
    public ModelAndView createGet() {
        ModelAndView mnv = new ModelAndView("buildingPlacement-create", "buildingPlacement", new BuildingPlacement());

        mnv.addObject("types", Type.values());
        fillUsers(mnv);
        BuildingPlacementController.fillCities(mnv);

        return mnv;
    }

    @RequestMapping(value="/request/create", method=RequestMethod.POST)
    public ModelAndView create(
            @ModelAttribute @Valid BuildingPlacement buildingPlacement,
            final BindingResult result,
            final RedirectAttributes redirectAttributes,
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file,
            @RequestParam("ppt") MultipartFile ppt
    ) throws IOException {

        if (result.hasErrors()) {
            createGet();
        }

        // TODO: UID get random
        String fileName = (new Date()).getTime() +"_"+ file.getOriginalFilename();
        FileWriter.writeMultipartFile(file,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + fileName);
        buildingPlacement.setPictureUri(fileName);

        // TODO: UID get random
        String pptName = (new Date()).getTime() +"_"+ ppt.getOriginalFilename();
        FileWriter.writeMultipartFile(ppt,
                request.getSession().getServletContext().getRealPath("/uploads/") + "/"
                        + pptName);
        buildingPlacement.setPresentationUri(pptName);

        buildingPlacementService.create(buildingPlacement);

        String message = "New buildingPlacement record "
                + buildingPlacement.getText()+" was successfully created.";
        redirectAttributes.addFlashAttribute("message", message);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("redirect:/request");

        return mav;
    }

    @RequestMapping(value="/buildingPlacement/show/{id}", method=RequestMethod.GET)
    public ModelAndView show(@PathVariable Integer id) {

        ModelAndView mav = new ModelAndView("buildingPlacement-show");

        BuildingPlacement buildingPlacement = buildingPlacementService.findById(id);
        mav.addObject("item", buildingPlacement);

        return mav;
    }
}
