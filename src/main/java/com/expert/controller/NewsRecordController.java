package com.expert.controller;

import com.expert.model.NewsRecord;
import com.expert.model.User;
import com.expert.service.IService;
import com.expert.validation.FileValidator;
import com.expert.validation.RecordPropertyEditor;
import com.expert.validation.NewsRecordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Controller
@RequestMapping(value="/news")
public class NewsRecordController {

	@Autowired
    @Qualifier("newsService")
	private IService<NewsRecord> newsService;

	@Autowired
	private NewsRecordValidator newsValidator;

    @Autowired
    FileValidator fileValidator;

    @Autowired
    private RecordPropertyEditor newsPropertyEditor;

    /**

     * Testing sandbox method according to http://docs.spring.io/spring/docs/3.1.x/spring-framework-reference/html/mvc.htm
     * "It may already be in the model due to an @ModelAttribute method in the same controller."
     */
    @ModelAttribute
    public NewsRecord newsRecordConstructor(
            @RequestParam(value="id", required=false) Integer idPost,
            HttpServletRequest request) {
        String[] uriParts = request.getRequestURI().split("/");

        Integer id = null;
        try {
            id = Integer.valueOf(uriParts[uriParts.length-1]);
        } catch (NumberFormatException ex) {

        }

        return (id == null) ? new NewsRecord() : newsService.findById(id);
    }

	@InitBinder
	private void initBinder(WebDataBinder binder) {
//		binder.setValidator(newsValidator);
        binder.registerCustomEditor(User.class, newsPropertyEditor);
	}

	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView("news-list");
        Collection<NewsRecord> news = newsService.findAll();
		mav.addObject("newsRecords", news);
		return mav;
	}
	
	@RequestMapping(value="/show/{id}", method=RequestMethod.GET)
	public ModelAndView editGet(@PathVariable Integer id) {

		ModelAndView mav = new ModelAndView("news-show");

		NewsRecord newsRecord = newsService.findById(id);
		mav.addObject("newsRecord", newsRecord);

        return mav;
	}
}
