package com.expert.controller;

import com.expert.form.BuildingPlacementFilterForm;
import com.expert.model.*;
import com.expert.service.IService;
import com.expert.service.TradingPlacementService;
import com.expert.util.UriBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;
import java.util.Collection;

@Controller
public class NavigationController {

    @Autowired
    private EntityManagerFactory emf;

    @Autowired
    private IService<User> userService;

    @Autowired
    private IService<NewsRecord> newsRecordService;

    @Autowired
    private IService<Slide> slideService;

    @Autowired
    private TradingPlacementService tradingPlacementService;

    @RequestMapping(value = {"/", "index"}, method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView mnv = new ModelAndView("index");

        Collection<Slide> slides = slideService.findAll();
        mnv.addObject("slides", slides);

        Collection<NewsRecord> news = newsRecordService.findAll();
        mnv.addObject("newsRecords", news);

        return mnv;
    }

    @RequestMapping(value = {"about"}, method = RequestMethod.GET)
    public ModelAndView about() {
        ModelAndView mnv = new ModelAndView("about");
        mnv.addObject("stuff", userService.findAll());

        return mnv;
    }

    @RequestMapping(value = {"service"}, method = RequestMethod.GET)
    public ModelAndView service() {
        ModelAndView mnv = new ModelAndView("service");

        return mnv;
    }

    @RequestMapping(value = {"realty"}, method = RequestMethod.GET)
    public ModelAndView realty() {
        ModelAndView mnv = new ModelAndView("realty");

        return mnv;
    }

    /**
     * Icon-view list of commerce
     */
    @RequestMapping(value = {"commerce"}, method = RequestMethod.GET)
    public ModelAndView commerce() {
        ModelAndView mnv = new ModelAndView("commerce");

        mnv.addObject("types", Type.commerce());
        fillBuildings(mnv,
                "SELECT bPl FROM BuildingPlacement bPl " +
                        "WHERE TYPE(bPl) IN (OfficePlacement, StoragePlacement, TradingPlacement)");

        return mnv;
    }

    /**
     * List of commerce
     */
    @RequestMapping(value = {"commerce/list"}, method = RequestMethod.GET)
    public ModelAndView commercyList(@ModelAttribute BuildingPlacementFilterForm buildingPlacementForm) {
        String query = UriBuilder.buildQuery(buildingPlacementForm);

        ModelAndView mnv = new ModelAndView("redirect:/"
                + buildingPlacementForm.getType().getUri() +"/list?" + query);

        return mnv;
    }

    /**
     * Icon-view list of personal
     */
    @RequestMapping(value = {"personal"}, method = RequestMethod.GET)
    public ModelAndView personal() {
        ModelAndView mnv = new ModelAndView("personal");

        mnv.addObject("types", Type.commerce());
        fillBuildings(mnv,
                "SELECT bPl FROM BuildingPlacement bPl " +
                        "WHERE TYPE(bPl) IN (ApartmentPlacement, HomePlacement, LandPlacement)");

        return mnv;
    }

    /**
     * List of personal
     */
    @RequestMapping(value = {"personal/list"}, method = RequestMethod.GET)
    public ModelAndView personalList(@ModelAttribute BuildingPlacementFilterForm buildingPlacementForm) {
        String query = UriBuilder.buildQuery(buildingPlacementForm);

        ModelAndView mnv = new ModelAndView("redirect:/"
                + buildingPlacementForm.getType().getUri() +"/list?" + query);

        return mnv;
    }

    @RequestMapping(value = {"request"}, method = RequestMethod.GET)
    public ModelAndView request() {
        ModelAndView mnv = new ModelAndView("request");

        mnv.addObject("types", Type.values());
        fillBuildings(mnv,
                "SELECT bPl FROM BuildingPlacement bPl " +
                        "WHERE type IS NOT NULL");

        return mnv;
    }

    /**
     * List of commerce
     */
    @RequestMapping(value = {"request/list"}, method = RequestMethod.GET)
    public ModelAndView requestList() {
        ModelAndView mnv = new ModelAndView("buildingPlacement-list");
        fillBuildings(mnv,
                "SELECT bPl FROM BuildingPlacement bPl " +
                        "WHERE type IS NOT NULL");

        return mnv;
    }

    @RequestMapping(value = {"apartment"}, method = RequestMethod.GET)
    public ModelAndView apartment() {
        ModelAndView mnv = new ModelAndView("apartment");

        return mnv;
    }

    @RequestMapping(value = {"contact"}, method = RequestMethod.GET)
    public ModelAndView contact() {
        ModelAndView mnv = new ModelAndView("contact");

        return mnv;
    }

    @RequestMapping(value = {"admin"}, method = RequestMethod.GET)
    public ModelAndView admin() {
        ModelAndView mnv = new ModelAndView("admin");

        return mnv;
    }

    private void fillBuildings(ModelAndView mnv, String query) {
        // form building params
        BuildingPlacementFilterForm buildingPlacementForm = new BuildingPlacementFilterForm();
        mnv.addObject("buildingPlacementForm", buildingPlacementForm);
        BuildingPlacementController.fillCities(mnv);

        EntityManager em = emf.createEntityManager();
        Collection<BuildingPlacement> buildingPlacements = em.createQuery(query).getResultList();
        mnv.addObject("items", buildingPlacements);
    }
}
