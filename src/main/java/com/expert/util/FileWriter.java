package com.expert.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FileWriter {
    public static void writeMultipartFile(MultipartFile file, String path) throws IOException {
        InputStream inputStream = file.getInputStream();

        File newFile = new File(path);
        if (!newFile.exists()) {
            newFile.createNewFile();
        }

        OutputStream outputStream = new FileOutputStream(newFile);

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
    }
}
