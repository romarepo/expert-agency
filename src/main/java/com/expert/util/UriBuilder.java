package com.expert.util;

import com.expert.form.BuildingPlacementFilterForm;

public class UriBuilder {

    public static String buildQuery(BuildingPlacementFilterForm buildingPlacementForm) {
        String query = "";

        if (buildingPlacementForm.getUser() != null) {
            query += "user=" + buildingPlacementForm.getUser() + "&";
        }

        if (buildingPlacementForm.getCity() != null) {
            query += "city=" + buildingPlacementForm.getCity() + "&";
        }

        if (buildingPlacementForm.getDistrict() != null) {
            query += "district=" + buildingPlacementForm.getDistrict() + "&";
        }

        if (buildingPlacementForm.getStreet() != null) {
            query += "street=" + buildingPlacementForm.getStreet() + "&";
        }

        if (buildingPlacementForm.getBuilding() != null) {
            query += "building=" + buildingPlacementForm.getBuilding() + "&";
        }

        if (buildingPlacementForm.getMinSquare() != null) {
            query += "minSquare=" + buildingPlacementForm.getMinSquare() + "&";
        }
        if (buildingPlacementForm.getMaxSquare() != null) {
            query += "maxSquare=" + buildingPlacementForm.getMaxSquare() + "&";
        }

        if (buildingPlacementForm.getMinPrice() != null) {
            query += "minPrice=" + buildingPlacementForm.getMinPrice() + "&";
        }
        if (buildingPlacementForm.getMaxPrice() != null) {
            query += "maxPrice=" + buildingPlacementForm.getMaxPrice() + "&";
        }

        if (buildingPlacementForm.getText() != null) {
            query += "text=" + buildingPlacementForm.getText() + "&";
        }

        return query;
    }
}
