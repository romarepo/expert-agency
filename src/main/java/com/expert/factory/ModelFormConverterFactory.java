package com.expert.factory;

import com.expert.model.IModel;
import com.expert.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

public final class ModelFormConverterFactory implements ConverterFactory<String, IModel> {

    public <T extends IModel> Converter<String, T> getConverter(Class<T> clazz) {
        return new StringToModelConverter<>();
    }

    private final class StringToModelConverter<T extends IModel> implements Converter<String, T> {

        @Autowired
        private IService<T> daoService;

        public StringToModelConverter() {

        }

        public T convert(String source) {
            return daoService.findById(Integer.parseInt(source));
        }
    }
}