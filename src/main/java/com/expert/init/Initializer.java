package com.expert.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class Initializer implements WebApplicationInitializer {

//    public static final String[] SPRING_CONFIG = {
//            "spring/batch/config/database.xml",
//            "spring/batch/config/context.xml",
//            "spring/batch/jobs/job-report.xml"
//        };
	private static final String DISPATCHER_SERVLET_NAME = "dispatcher";

	public void onStartup(ServletContext servletContext)
			throws ServletException {

//        GenericXmlApplicationContext xmlContext = new GenericXmlApplicationContext();
//        xmlContext.getEnvironment().setActiveProfiles("standalone");
//        xmlContext.load(SPRING_CONFIG);
//        xmlContext.refresh();

		AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();
        webContext.register(WebAppConfig.class);

		servletContext.addListener(new ContextLoaderListener(webContext));

        webContext.setServletContext(servletContext);

		Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME,
				new DispatcherServlet(webContext));

		servlet.addMapping("/");
		servlet.setLoadOnStartup(1);
	}

}
